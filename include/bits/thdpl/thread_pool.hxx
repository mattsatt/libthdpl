#ifndef LIBTHDPL_THREAD_POOL
#define LIBTHDPL_THREAD_POOL

#include <condition_variable>
#include <future>
#include <mutex>
#include <queue>
#include <thread>
#include <vector>
#include "thread_job.hxx"

namespace stdx::thdpl {
    /// @brief Thread pool for queueing tasks.
    /// @tparam T The function type.
    template <typename T>
    class thread_pool;

    /// @brief Thread pool for queueing tasks.
    /// @tparam R The function return type.
    /// @tparam Args The function argument types
    template <typename R, typename... Args>
    class thread_pool<R(Args...)> {
    public:
        /// @brief The job type.
        using job_type = thread_job<R(Args...)>;

        /// @brief Destructs a thread_pool.
        ~thread_pool() {
            stop();
        }

        /// @brief Checks if there are any pending jobs.
        bool has_jobs() const {
            std::unique_lock lock(mtx_);
            return !jobs_.empty();
        }

        /// @brief Chceks if the thread pool is running.
        bool is_running() const {
            std::unique_lock lock(mtx_);
            return running_;
        }

        /// @brief Queues a new job on the thread pool.
        /// @tparam F The job type.
        /// @param function The job.
        /// @param args The job arguments.
        /// @return A future.
        template <typename F>
        std::future<R> queue_job(F&& function, Args&&... args) {
            std::future<R> future(make_job(std::forward<F>(function), std::forward<Args>(args)...));
            cvar_.notify_one();
            return future;
        }

        /// @brief Start the thread pool.
        /// @param num_threads The number of threads.
        void start(std::size_t num_threads = std::thread::hardware_concurrency()) {
            if (!is_running()) {
                set_running(true);
                for (std::size_t i = 0; i < num_threads; ++i) {
                    threads_.push_back(std::thread(&thread_pool::poll, this));
                }
            }
        }

        /// @brief Stop the thread pool.
        void stop() {
            if (is_running()) {
                set_running(false);
                cvar_.notify_all();
                for (auto& thread : threads_) {
                    thread.join();
                }
                threads_.clear();
            }
        }

    private:
        /// @brief The threads available.
        std::vector<std::thread> threads_;

        /// @brief The queue of jobs waiting.
        std::queue<job_type> jobs_;

        /// @brief The mutex for jobs and running check.
        mutable std::mutex mtx_;

        /// @brief The condition variable to notify threads of jobs.
        std::condition_variable cvar_;

        /// @brief If the thread pool is running.
        bool running_ = false;

        /// @brief Queues a new job on the thread pool.
        /// @tparam F The job type.
        /// @param function The job.
        /// @param args The job arguments.
        /// @return A future.
        template <typename F>
        std::future<R> make_job(F&& function, Args&&... args) {
            std::unique_lock lock(mtx_);
            std::packaged_task task(std::forward<F>(function));
            std::tuple targs(std::make_tuple(std::forward<Args>(args)...));
            jobs_.push(job_type(std::move(task), std::move(targs)));
            return jobs_.back().task.get_future();
        }

        /// @brief Polls the job queue for available jobs.
        void poll() {
            while (true) {
                std::unique_lock lock(mtx_);
                cvar_.wait(lock, [this] { return !jobs_.empty() || !running_; });
                if (!running_) { return; }
                job_type job(std::move(jobs_.front()));
                jobs_.pop();
                lock.unlock();
                std::apply(job.task, job.args);
            }
        }

        /// @brief Convenience method for setting the running flag.
        /// @param running The running value.
        void set_running(bool running) {
            std::unique_lock lock(mtx_);
            running_ = running;
        }
    };
}

#endif
