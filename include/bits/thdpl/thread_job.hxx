#ifndef LIBTHDPL_THREAD_JOB
#define LIBTHDPL_THREAD_JOB

#include <future>
#include <tuple>

namespace stdx::thdpl {
    /// @brief An asynchronous job for the thread pool.
    /// @tparam T The job type.
    template <typename T>
    class thread_job;

    /// @brief An asynchronous job for the thread pool.
    /// @tparam R The return type.
    /// @tparam Args The argument types. 
    template <typename R, typename... Args>
    class thread_job<R(Args...)> {
    public:
        /// @brief The task to execute.
        std::packaged_task<R(Args...)> task;

        /// @brief The task arguments.
        std::tuple<Args...> args;
    };
}

#endif
