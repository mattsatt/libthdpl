CXX := g++
CXXFLAGS := -Wall -Wextra -std=c++20
LDFLAGS := -I include

BIN_DIR := bin
BUILD_DIR := build
TEST_DIR := test

SOURCES := $(wildcard $(TEST_DIR)/*.cxx)
OBJECTS := $(SOURCES:$(TEST_DIR)/%.cxx=$(BUILD_DIR)/%.o)
TARGETS := $(OBJECTS:$(BUILD_DIR)/%.o=$(BIN_DIR)/%)

INSTALL_DIR := /usr/local/include
STDX_DIR := $(INSTALL_DIR)/stdx

.PHONY: all clean install

all: $(TARGETS)

clean:
	@rm -rf $(BIN_DIR)
	@rm -rf $(BUILD_DIR)

install:
	@mkdir -p $(STDX_DIR)
	@cp -T include/thdpl.hxx $(STDX_DIR)/thdpl
	@cp -R include/bits $(STDX_DIR)

$(BIN_DIR)/%: $(BUILD_DIR)/%.o
	@mkdir -p $(BIN_DIR)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o $@ $^

$(BUILD_DIR)/%.o: $(TEST_DIR)/%.cxx
	@mkdir -p $(BUILD_DIR)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -c -o $@ $<
