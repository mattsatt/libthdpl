#include <cassert>
#include <iostream>
#include <thread>
#include <thdpl.hxx>

using namespace stdx::thdpl;

auto make_thread_job(int id) {
    return [id] {
        std::this_thread::sleep_for(std::chrono::seconds(id));
        return id;
    };
}

int main() {
    thread_pool<int()> pool;
    pool.start();

    auto f1 = pool.queue_job(make_thread_job(1));
    auto f2 = pool.queue_job(make_thread_job(2));
    auto f3 = pool.queue_job(make_thread_job(3));

    assert(f1.valid() && f1.get() == 1);
    assert(f2.valid() && f2.get() == 2);
    assert(f3.valid() && f3.get() == 3);

    return 0;
}
